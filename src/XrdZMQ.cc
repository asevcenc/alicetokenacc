#include "XrdZMQ.hh"
#include "XrdAliceTokenAcc.hh"
#include "XrdOuc/XrdOucEnv.hh"
#include "XrdSec/XrdSecEntity.hh"

#include <thread>
#include <chrono>

//------------------------------------------------------------------------------
// Constructor
//------------------------------------------------------------------------------
BusyLockMonitor::BusyLockMonitor(XrdZMQ* xrd_zmq, size_t index):
  mIndex(index), mXrdZmq(xrd_zmq)
{
  std::unique_lock<std::mutex> lock(*(mXrdZmq->mMutex[mIndex]));

  if (mXrdZmq->mBusy[mIndex] == false) {
    mXrdZmq->mBusy[mIndex] = true;
    mLocked = true;
  }
}

//------------------------------------------------------------------------------
// Destructor
//------------------------------------------------------------------------------
BusyLockMonitor::~BusyLockMonitor()
{
  // Free the processor
  if (mLocked) {
    std::unique_lock<std::mutex> lock(*(mXrdZmq->mMutex[mIndex]));
    mXrdZmq->mBusy[mIndex] = false;
  }
}

//------------------------------------------------------------------------------
// Constructor
//------------------------------------------------------------------------------
XrdZMQ::XrdZMQ(XrdAliceTokenAcc& acc, int parallelism, int basesocket):
  mAcc(acc), mParallelism(parallelism),
  mBasesocket(basesocket), mCnt(0ull)
{
  // empty
}

//------------------------------------------------------------------------------
// Destructor
//------------------------------------------------------------------------------
XrdZMQ::~XrdZMQ()
{
  StopServer();

  for ( int i = 0 ; i < mParallelism; i++ ) {
    delete mSocket[i];
    delete mContext[i];
    delete mMutex[i];
  }
}

//------------------------------------------------------------------------------
// Server loop
//------------------------------------------------------------------------------
bool
XrdZMQ::RunServer()
{
  mPid = getpid();
  for (int i = 0 ; i< mParallelism; ++i) {
    pid_t pid;
    if ( !(pid = fork()) ) {
      // forked process goes here
      zmq::context_t context (1);
      zmq::socket_t socket (context, ZMQ_REP);
      std::string zmqaddr = "tcp://*:" + std::to_string(mBasesocket + i);
      fprintf(stderr,"# XrdZMQ::addr binding on [%s]\n", zmqaddr.c_str());
      socket.bind (zmqaddr);
      while (true) {
        zmq::message_t request;
        //  Wait for next request from client
        socket.recv (&request);
        std::string encoded((char*)request.data(), request.size());
        XrdSecEntity entity;
        std::string path;
        Access_Operation oper;
        std::string authz;
        int rc = 0;
        if (getenv("ALICETOKENDEBUG")) {fprintf(stderr,"# XrdZMQ::Encoded [ %s ]\n", encoded.c_str());}
        if (!mAcc.DecodeAccess(encoded, entity, path, oper, authz)) {
          if (getenv("ALICETOKENDEBUG")) {fprintf(stderr,"# XrdZMQ::Decode Access failed\n");}
          rc = -1;
        } else {
          std::string sauthz="&authz=";
          sauthz += authz;
          if (getenv("ALICETOKENDEBUG")) {fprintf(stderr,"# XrdZMQ::AccessExec for '%s' '%s'\n", path.c_str(),sauthz.c_str());}
          XrdOucEnv authzEnv(sauthz.c_str());

          rc = mAcc.AccessExec(&entity,
                               path.c_str(),
                               oper,
                               &authzEnv);
        }

        std::string src = std::to_string(rc);
        //  Send reply back to client
        zmq::message_t reply (src.length()+1);
        memcpy ((void *) reply.data (), (void*)src.c_str(), src.length()+1);
        socket.send (reply);
        if (kill(mPid,0)) {
          fprintf(stderr,"# XrdZMQ::kill parent disappeared - exiting ...\n");
          exit(-1);
        }
      }
      exit(0);
    } else {
      // track the child processes
      mPids.push_back(pid);
    }
  }

  // test that all server are up
  for (int i = 0 ; i< mParallelism; ++i) {
    if (kill(mPids[i],0)) {
      return false;
    }
  }
  return true;
}

bool
XrdZMQ::SetupClients()
{
  mBusy.reset(new bool[mPids.size()]);

  try {
    for (int i= 0; i < mPids.size(); ++i) {
      // create connections
      zmq::context_t* ctx = new zmq::context_t(1);
      zmq::socket_t * socket = new zmq::socket_t(*ctx, ZMQ_REQ);
      mContext.push_back(ctx);
      mSocket.push_back(socket);
      mMutex.push_back(new std::mutex());
      mBusy[i] = false;
      std::string zmqaddr = "tcp://localhost:" + std::to_string(mBasesocket + i);
      fprintf(stderr,"# XrdZMQ::client connecting to [%s]\n", zmqaddr.c_str());
      mSocket[i]->connect(zmqaddr);
    }
  } catch (...) {
    return false;
  }
  return true;
}

void XrdZMQ::StopServer()
{
  for (int i = 0 ; i< mPids.size(); i++) {
    kill(mPids[i],9);
    wait(NULL);
  }
}

int XrdZMQ::Send(std::string& msg)
{
  static bool debug_enabled = (getenv("ALICETOKENDEBUG") != 0);
  size_t loops = 0;

  do {
    size_t index = (++mCnt) % mParallelism;
    loops++;

    if (!(loops % mParallelism)) {
      // snooze a bit everytime we checked out all slots
      std::this_thread::sleep_for(std::chrono::milliseconds(100));
      if (loops % 100 == 0) {
        fprintf(stderr,"# XrdZMQ::client looping since %.02f seconds ...\n", loops*100.0 / 1000.0);
      }
    }

    BusyLockMonitor monitor (this, index);

    if (!monitor.IsLocked()) {
      continue;
    }

    zmq::message_t request (msg.c_str(), msg.size());

    if (debug_enabled) {
      fprintf(stderr,"XrdZMQ::send to %u %s\n", index, msg.c_str());
    }

    try {
      mSocket[index]->send (request);
    } catch (const zmq::error_t& e) {
      fprintf(stderr, "msg=\"zmq send exception\" what=\"%s\" errno=%d",
              e.what(), e.num());
      // Return an error to the calling thread
      return -1;
    }

    zmq::message_t reply;

    try {
      mSocket[index]->recv (&reply);
    } catch (const zmq::error_t& e) {
      fprintf(stderr, "msg=\"zmq receive exception\" what=\"%s\" errno=%d",
              e.what(), e.num());
      // Return an error to the calling thread
      return -1;
    }

    if (debug_enabled) {
      fprintf(stderr,"XrdZMQ::reply %s [%d]\n", reply.data(), reply.size());
    }

    std::string response((char*)reply.data(),reply.size());
    return std::atoi(response.c_str());
  } while(true);
}
