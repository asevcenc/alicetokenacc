#include <unistd.h>
#include <stdio.h>
#include <time.h>
#include <signal.h>
#include <vector>
#include <sys/types.h>
#include <sys/wait.h>
#include <zmq.hpp>
#include <string>
#include <iostream>
#include <atomic>
#include <mutex>
#include <memory>

#pragma once

class XrdAliceTokenAcc;
class XrdZMQ;

//------------------------------------------------------------------------------
//! Class BusyLockMonitor
//------------------------------------------------------------------------------
class BusyLockMonitor
{
public:
  //----------------------------------------------------------------------------
  //! Constructor
  //----------------------------------------------------------------------------
  BusyLockMonitor(XrdZMQ* xrd_zmq, size_t index);

  //----------------------------------------------------------------------------
  //! Destructor
  //----------------------------------------------------------------------------
  ~BusyLockMonitor();

  //----------------------------------------------------------------------------
  //! Check if monitor managed to take the lock
  //----------------------------------------------------------------------------
  inline bool IsLocked() const {
    return mLocked;
  }

private:
  size_t mIndex; ///< Index in the mMutex vector
  XrdZMQ* mXrdZmq; ///< Pointer the main class
  bool mLocked {false}; ///< Flag to mark successful lock
};


//------------------------------------------------------------------------------
//! Class XrdZMQ
//------------------------------------------------------------------------------
class XrdZMQ  {
  friend class BusyLockMonitor;

public:
  //----------------------------------------------------------------------------
  //! Constructor
  //----------------------------------------------------------------------------
  XrdZMQ(XrdAliceTokenAcc& acc, int parallelism=16, int basesocket=6000);

  //----------------------------------------------------------------------------
  //! Destructor
  //----------------------------------------------------------------------------
  virtual ~XrdZMQ();

  bool RunServer();

  void StopServer();

  bool SetupClients();

  int Send(std::string& msg);

private:
  // robin counter to select client socket
  std::atomic<uint64_t> mCnt;
  // pids of forked server
  std::vector<pid_t> mPids;
  // client contexts
  std::vector<zmq::context_t*> mContext;
  // client sockets
  std::vector<zmq::socket_t*> mSocket;
  // client mutex
  std::vector<std::mutex*> mMutex;
  // client busy
  std::unique_ptr<bool[]> mBusy {nullptr};
  pid_t mPid;
  int mParallelism;
  int mBasesocket;
  XrdAliceTokenAcc& mAcc;
};
