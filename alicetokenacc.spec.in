#-------------------------------------------------------------------------------
# Helper macros and variables
#-------------------------------------------------------------------------------
%define _unpackaged_files_terminate_build 0
%define xrootd_version_min @XROOTD_VERSION_MIN@
%define devtoolset devtoolset-8

# By default we build without AddressSanitizer. To enable it,
# pass the "--with asan" flag to the rpmbuild command
%bcond_without asan

#-------------------------------------------------------------------------------
# Package definitions
#-------------------------------------------------------------------------------
Summary: ALICE Token Authorization plugin
Name: @CPACK_PACKAGE_NAME@
Version: @CPACK_PACKAGE_VERSION@
Release: @CPACK_PACKAGE_RELEASE@%{dist}%{?_with_asan:.asan}
Prefix: /usr
License: GPLv2+
Group: Applications/File
Vendor: CERN
Source: %{name}-%{version}-@CPACK_PACKAGE_RELEASE@.tar.gz

# List build requirements
BuildRequires: xrootd-server-devel >= %{xrootd_version_min}
BuildRequires: xrootd-client-devel >= %{xrootd_version_min}
BuildRequires: xrootd-private-devel >= %{xrootd_version_min}
BuildRequires: libcurl, libcurl-devel
BuildRequires: openssl, openssl-devel
BuildRequires: libxml2,  libxml2-devel
BuildRequires: zeromq, zeromq-devel, cppzmq-devel
# Install
%if 0%{?rhel} == 7
BuildRequires: cmake3 >= 3.17
BuildRequires: %{devtoolset}
BuildRequires: %{devtoolset}-binutils-devel
%define cmake cmake3
%else
BuildRequires: cmake >= 3.17
%define cmake cmake
%endif

# For asan/tsan builds we need devtoolset-9
%if %{?_with_asan:1}%{!?_with_asan:0} || %{?_with_tsan:1}%{!?_with_tsan:0}
%define devtoolset devtoolset-9
%endif

%if %{?_with_asan:1}%{!?_with_asan:0}
%if %{?rhel} == 7
BuildRequires: libasan5, %{devtoolset}-libasan-devel
Requires: libasan5
%else
BuildRequires: libasan
Requires: libasan
%endif
%endif

Requires: libcurl, libxml2, openssl, zeromq

%description
ALICE Token Authorization plug-in

%prep
%setup -n %{name}-%{version}-@CPACK_PACKAGE_RELEASE@
%global build_flags %{nil}

%if %{?_with_asan:1}%{!?_with_asan:0}
%global build_flags %{build_flags} -DASAN=1
%endif

%build
test -r $RPM_BUILD_ROOT && rm -r $RPM_BUILD_ROOT
mkdir -p build
cd build

%if 0%{?rhel} == 7
source /opt/rh/%{devtoolset}/enable
%endif

%{cmake} ../ -DCMAKE_BUILD_TYPE=RelWithDebInfo %{build_flags}
%{__make} %{_smp_mflags}

%install
cd build
%{__make} install DESTDIR=$RPM_BUILD_ROOT

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%dir %{_sysconfdir}/grid-security/
%defattr(400,daemon,daemon,755)
%dir %{_sysconfdir}/grid-security/xrootd/
%{_sysconfdir}/grid-security/xrootd/privkey.pem
%{_sysconfdir}/grid-security/xrootd/pubkey.pem
%config(noreplace) %{_sysconfdir}/grid-security/xrootd/TkAuthz.Authorization
%{_libdir}/libXrdAliceTokenAcc.so
