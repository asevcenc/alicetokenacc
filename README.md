# alicetokenacc

## Description

This is a CMAKE repackaged version of alice token authorization supporting to run with multi process to better scale asymmetric decoding performance with number of cores.

## Configuration

You can enable it for the Ofs library including the following tags into your xrootd configuration file:

ofs.authlib /usr/lib64/libXrdAliceTokenAcc.so
ofs.authorize 1

You enable the multi-process feature by adding to the xrootd configuration file with a value > 0

# Run 16 authz processes
alicetokenacc.multiprocess 16

## How to build this library
mkdir build
cd build
cmake ../
# compile and install
make -j 4 install
# create an rpm
make package

## Changes

* refactored build infrastructure and joined TokenAuthz + XrdAliceTokenAcc packages into a single package
* added multiprocess feature
* fixed SEGV when plugin was called with not base64 encoded authorization info








