#!/usr/bin/env bash

EOS_CODENAME="diopside"
STCI_ROOT_PATH="/eos/project/s/storage-ci/www/eos"

for BUILD_TYPE in "el-7" "el-8" "el-9" "el-9-asan"; do
    EXPORT_DIR_RPMS=${STCI_ROOT_PATH}/${EOS_CODENAME}-depend/${BUILD_TYPE}/x86_64/
    EXPORT_DIR_SRPMS=${STCI_ROOT_PATH}/${EOS_CODENAME}-depend/${BUILD_TYPE}/SRPMS/
    echo "Publishing for: ${BUILD_TYPE} in location: ${EXPORT_DIR_RPMS}"
    mkdir -p ${EXPORT_DIR_RPMS}
    mkdir -p ${EXPORT_DIR_SRPMS}
    cp ${BUILD_TYPE}/RPMS/x86_64/*.rpm ${EXPORT_DIR_RPMS}
    cp ${BUILD_TYPE}/SRPMS/*.src.rpm ${EXPORT_DIR_SRPMS}
    createrepo -q ${EXPORT_DIR_RPMS}
done
